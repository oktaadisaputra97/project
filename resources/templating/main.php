<?php
    require_once dirname(__FILE__)."/../../class/config.php";
    
    class Template{
        public $pageTitle = "UKM TABUH BRAMARA GITA";
        public $contentTitle;
        public $contentDescription;
        public $activePage;
        //public $logo="../images/icon-bhumiku.png";
        public $boxTitle;
        public $boxFooter;
        
        //Start Content
        public function startContent(){
            
            $this->startHtml();
            echo $this->headerTemplate();
            echo $this->sidebarTemplate();
            echo $this->contentTemplate();
        }
        
        //End Content
        public function endContent(){
            echo $this->footerTemplate();
        }
        
        //Start HTML
        public function startHtml(){
            echo "
                <!DOCTYPE html>
                <html>
                <head>
                  <meta charset='utf-8'>
                  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                  <title>$this->pageTitle</title>
                  <!-- Tell the browser to be responsive to screen width -->
                  <meta name='viewport' content='width=device-width, initial-scale=1'>

                  <!-- Font Awesome -->
                  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
                  <!-- Ionicons -->
                  <link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
                  <!-- Theme style -->
                  <link rel='stylesheet' href='".MAIN_URL."/public/dist/css/adminlte.min.css'>
                  <!-- Google Font: Source Sans Pro -->
                  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700' rel='stylesheet'>
                  <!-- DataTables -->
                  <link rel='stylesheet' href='".MAIN_URL."/public/plugins/newdatatables/datatables.css'>
                </head>
            
            ";
        }
        
        public function endBody(){
            echo "</body>";
        }


        public function endHtml(){
            echo "</html>";
        }

        private function contentTemplate(){
            ob_start();
            include "content.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function headerTemplate(){
            ob_start();
            include "header.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function footerTemplate(){
            ob_start();
            include "footer.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function sidebarTemplate(){
            ob_start();
            include "sidebar.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }
    }
?>
