</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= MAIN_URL ?>/public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= MAIN_URL ?>/public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="<?= MAIN_URL ?>/public/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= MAIN_URL ?>/public/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= MAIN_URL ?>/public/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= MAIN_URL ?>/public/dist/js/demo.js"></script>
<!-- Data Table -->
<script type="text/javascript" src="<?= MAIN_URL ?>/public/plugins/newdatatables/datatables.min.js"></script>