<div class="card">
  <div class="card-header">
    <h3 class="card-title">Jadwal Latihan Rutin</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body p-0">
    <table class="table table-striped">
      <tr>
        <th style="width: 10px">No.</th>
          <th>Hari</th>
            <th>Jam</th>
              <th style="width: 40px">Tempat</th>
      </tr>
        <tr>
          <td>1.</td>
            <td>Rabu</td>
               <td>
                  15.00
                </td>
                   <td>Art Center</td>
        </tr>
          <tr>
            <td>2.</td>
              <td>Minggu</td>
                <td>
                  09.00
                </td>
                   <td>Lab. Seni Budaya</td>
        </tr>
    </table>
  <!-- /.card-body -->
  </div>
<!-- /.card -->
</div>