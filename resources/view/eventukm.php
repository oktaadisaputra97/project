<div class="card">
  <div class="card-header">
    <h3 class="card-title">Event UKM</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body p-0">
    <table class="table table-striped">
      <tr>
        <th style="width: 10px">No.</th>
          <th>Event</th>
            <th>Tanggal</th>
              <th>Pukul</th>
                <th style="width: 40px">Tempat</th>
      </tr>
        <tr>
          <td>1.</td>
            <td>Bazzar UKM</td>
              <td>12 April 2018</td>
                <td>
                  15.00-23.00
                </td>
                  <td>Warung Pencar</td>
        </tr>
        <tr>
          <td>2.</td>
            <td>Pengabdian Masyarakat</td>
              <td>19 Mei 2018</td>
                <td>
                  08.00-11.00
                </td>
                  <td>SD Negeri 5 Penebel</td>
        </tr>
        <tr>
          <td>3.</td>
            <td>KBOR VII</td>
              <td>18 Agustus 2018</td>
                <td>
                  08.00-Selesai
                </td>
                  <td>Lap. Lumintang</td>
        </tr>
        <tr>
          <td>4.</td>
            <td>Pagelaran Akhir Tahun</td>
              <td>24 Desember 2018</td>
                <td>
                  19.00-21.00
                </td>
                  <td>Lap. Puputan Badung</td>
        </tr>

    </table>
  <!-- /.card-body -->
  </div>
<!-- /.card -->
</div>