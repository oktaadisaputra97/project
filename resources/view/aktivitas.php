<?php
	//Required files
	require_once dirname(__FILE__)."/../../class/config.php";
	require_once dirname(__FILE__)."/../../resources/templating/main.php";

	//config panggil
	$db = new Database();
	$db->connect();

	//manggil template
	$template = new Template();

	$template->pageTitle="Aktivitas";
	$template->contentTitle="Aktivitas Pengguna Aplikasi UKM";
	$template->startContent();
?>
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"><?= $template->contentDescription="Data Aktivitas"; ?></h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<table class="table display table-striped table-responsive" id="dataanggota">
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal</th>
						<th>Aktivitas</th>
						<th>Id User</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$no=0;
						//          nama table ,  nama colom yg bakal di select         , join, where clause
						$db->select("log_user");
						$result = $db->getResult();
						foreach($result as $show){
							$no++;
					?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $show['tanggal'] ?></td>
							<td><?= $show['aktivitas'] ?></td>
							<td><?= $show['id_user'] ?></td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>


<?php
	$template->endContent();
?>
<!-- Place Script Here (if available) -->

<!-- End Place -->
<?php 
	$template->endBody(); 
	$template->endHtml();
?>