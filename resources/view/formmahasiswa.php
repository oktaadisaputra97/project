<form role="form">
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputEmail1">NIM</label>
      <input type="text" class="form-control" id="nim" placeholder="Masukan NIM">
    </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Nama</label>
        <input type="text" class="form-control" id="nama" placeholder="Masukan Nama">
      </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Alamat</label>
          <input type="text" class="form-control" id="alamat" placeholder="Masukan Alamat">
        </div>
          <div class="form-group">
            <label for="exampleInputPassword1">No.Hp</label>
            <input type="text" class="form-control" id="no_hp" placeholder="Masukan No.Hp">
          </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Program Studi</label>
              <input type="text" class="form-control" id="prodi" placeholder="Masukan Prodi">
            </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Angkatan</label>
                <input type="text" class="form-control" id="angkatan" placeholder="Masukan Angkatan">
              </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Status</label>
                  <input type="text" class="form-control" id="status" placeholder="Masukan Status">
                </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>