<?php
	//Required files
	require_once dirname(__FILE__)."/../../class/config.php";
	require_once dirname(__FILE__)."/../../resources/templating/main.php";

	//config panggil
	$db = new Database();
	$db->connect();

	//manggil template
	$template = new Template();

	$template->pageTitle="Home UKM";
	$template->contentTitle="Dashboard UKM";
	$template->startContent();
?>

<div class="row">
	<div class="col-md-3 col-sm-6 col-12">
		<div class="info-box">
			<span class="info-box-icon bg-info"><i class="fa fa-user-plus"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Tambah Anggota</span>
				</div>
		 <!-- /.info-box-content -->
		</div>
	<!-- /.info-box -->
	</div>
	<!-- /.col -->
	<div class="col-md-3 col-sm-6 col-12">
		<div class="info-box">
			<span class="info-box-icon bg-success"><i class="fa fa-book"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Pembukuan Kas</span>
				</div>
			<!-- /.info-box-content -->
		</div>
	<!-- /.info-box -->
	</div>
	<!-- /.col -->
	<div class="col-md-3 col-sm-6 col-12">
		<div class="info-box">
			<span class="info-box-icon bg-warning"><i class="fa fa-calendar"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Jadwal Latihan Rutin</span>
				</div>
		<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	<!-- /.col -->
	<div class="col-md-3 col-sm-6 col-12">
		<div class="info-box">
			<span class="info-box-icon bg-danger"><i class="fa fa-star-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Event UKM</span>
				</div>
		<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	 <!-- /.col -->
 </div>

<?php
	$template->endContent();
?>
<!-- Place Script Here (if available) -->

<!-- End Place -->
<?php 
	$template->endBody(); 
	$template->endHtml();
?>