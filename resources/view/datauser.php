<?php
	//Required files
	require_once dirname(__FILE__)."/../../class/config.php";
	require_once dirname(__FILE__)."/../../resources/templating/main.php";

	//config panggil
	$db = new Database();
	$db->connect();

	//manggil template
	$template = new Template();

	$template->pageTitle="User";
	$template->contentTitle="Data Penguna Aplikasi UKM";
	$template->startContent();
?>
<!-- Place Content Here (Dinamis) -->
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"><?= $template->contentDescription="Data User"; ?></h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<table class="table table-striped table-responsive">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$no=0;
						//          nama table ,  nama colom yg bakal di select         , join, where clause
						$db->select("tb_user","nama,status",NULL,"status='1'");
						$result = $db->getResult();
						foreach($result as $show){
							$no++;
					?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $show['nama'] ?></td>
							<?php
								$status = $show['status'];
								if($status=="1"){
							?>
								<td>Aktif</td>
							<?php
								}else{
							?>
								<td>Tidak Aktif</td>
							<?php
								}
							?>
							<td>
								<button class="btn btn-navy" name="update" id="update">
									<i class="fa fa-edit"></i> 
									Update
								</button>
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.card-body -->
		<div class="card-footer">

		</div>
		<!-- /.card-footer-->
	</div>
</div>


<?php
	$template->endContent();
?>
<!-- Place Script Here (if available) -->

<!-- End Place -->
<?php 
	$template->endBody(); 
	$template->endHtml();
?>