<?php
	//Required files
	require_once dirname(__FILE__)."/../../class/config.php";
	require_once dirname(__FILE__)."/../../resources/templating/main.php";

	//config panggil
	$db = new Database();
	$db->connect();

	//manggil template
	$template = new Template();

	$template->pageTitle="Data Anggota";
	$template->contentTitle="Data Anggota UKM";
	$template->startContent();
?>
<!-- Place Content Here (Dinamis) -->
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"><?= $template->contentDescription="Data Mahasiswa"; ?></h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<table class="table display table-striped table-responsive" id="dataanggota">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No. Hp</th>
						<th>Program Studi</th>
						<th>Angkatan</th>
						<th>
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$no=0;
						//          nama table ,  nama colom yg bakal di select         , join, where clause
						$db->select("tb_angota","nim,nama,alamat,no_hp,idprodi,angkatan",NULL,"status='1'");
						$result = $db->getResult();
						foreach($result as $show){
							$no++;
					?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $show['nama'] ?></td>
							<td><?= $show['alamat'] ?></td>
							<td><?= $show['no_hp'] ?></td>
							<td><?= $show['idprodi'] ?></td>
							<td><?= $show['angkatan'] ?></td>
							<td>
								<button class="btn btn-block btn-outline-warning" name="update" id="update">
									<i class="fa fa-edit"></i> 
									Update
								</button>
							</td>
							<td>
								<button class="btn btn-block btn-outline-danger" name="delete" id="delete">
									<i class="fa fa-trash"></i> 
									Delete
								</button>
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.card-body -->
		<div class="card-footer">

		</div>
		<!-- /.card-footer-->
	</div>
</div>
<!-- End Content -->
<?php
	$template->endContent();
?>
<!-- Place Script Here (if available) -->

<script>
$(document).ready(function(){
	$("#dataanggota").dataTable();
});
</script>
<!-- End Place -->
<?php 
	$template->endBody(); 
	$template->endHtml();
?>